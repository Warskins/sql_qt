import configparser
import os
import logging

PATH = "settings.ini"


LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
logging.basicConfig(filename='log.txt',
                    level=logging.DEBUG,
                    format=LOG_FORMAT)
logger = logging.getLogger()


def createConfig(host, user, password, database):
    """
    Create a config file
    """
    config = configparser.ConfigParser()
    config.add_section("Settings")
    config.set("Settings", "host", host)
    config.set("Settings", "user", user)
    config.set("Settings", "password", password)
    config.set("Settings", "database", database)

    with open(PATH, "w") as config_file:
        config.write(config_file)


def get_config():
    """
    Returns the config object
    """
    if not os.path.exists(PATH):
        return None

    config = configparser.ConfigParser()
    config.read(PATH)
    return config


