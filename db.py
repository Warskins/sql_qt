import mysql.connector
from config import logger

where_patterns = {
    'Providers': 'WHERE provider_code = %s',
    'Individuals': 'WHERE provider_code = %s',
    'Entities': 'WHERE provider_code = %s',
    'Deliveries': 'WHERE contract_number=%s AND item=%s',
    'Contracts': 'WHERE contract_number = %s'
}

insert_patterns = {
    'Providers': '(provider_code, address, comment) VALUES (%s,%s,%s)',
    'Individuals': '(provider_code, last_name, first_name, patronymic, certificate_number) VALUES (%s,%s,%s,%s,%s)',
    'Entities': '(provider_code, name, tax_number, certificate_number) VALUES (%s,%s,%s,%s)',
    'Deliveries': '(contract_number, item, count, cost, comment) VALUES (%s,%s,%s,%s,%s)',
    'Contracts': '(contract_number, contract_date, provider_code, comments) VALUES (%s,%s,%s,%s)'
}


class DB:
    def __init__(self):
        self.connection = None
        self.is_connected = False
        self.cursor = None
        self.database_name = None

    def connect(self, host, user, password, database):
        try:
            self.connection = mysql.connector.connect(
                host=host,
                user=user,
                password=password,
                database=database)
            self.cursor = self.connection.cursor()
            self.is_connected = True
            self.database_name = self.connection.database
            return True
        except mysql.connector.Error as err:
            logger.warn(err)
            return False

    def disconnect(self):
        try:
            self.connection.close()
            self.cursor = None
            self.database_name = None
            self.is_connected = False
            return True
        except mysql.connector.Error as err:
            logger.warn(err)
            return False

    def get_tables(self):
        self.cursor.execute("SHOW TABLES")
        return self.cursor.fetchall()

    def insert(self, table_name, *args):
        sql = f'INSERT INTO {table_name} {insert_patterns[table_name]}'
        parameters = tuple(args)
        return self.__execute(sql, parameters)

    def delete(self, table_name, *args):
        sql = f"DELETE FROM {table_name} {where_patterns[table_name]}"
        parameters = tuple(args)
        return self.__execute(sql, parameters)

    def update(self, table_name, column_name, value, *args):
        sql = f"UPDATE {table_name} SET {column_name} ='{value}' {where_patterns[table_name]}"
        parameters = tuple(args)
        return self.__execute(sql, parameters)

    def view(self, table_name):
        self.cursor.execute(f"SELECT * FROM {table_name}")
        return self.cursor.column_names, self.cursor.fetchall()

    def __execute(self, sql, parameters):
        try:
            self.cursor.execute(sql, parameters)
            self.connection.commit()
            return True
        except mysql.connector.Error as err:
            logger.warn(err)
            return False


db = DB()
