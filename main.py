import sys
from PyQt5.QtWidgets import QApplication

from main_window import MainWindow



if __name__ == '__main__':
    app = QApplication([])
    application = MainWindow()
    application.show()
    sys.exit(app.exec())
