from PyQt5 import QtCore
from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem, QMenu, QInputDialog, QMessageBox

from db import db
from ui_py.table_window import Ui_TableWindow


class TableWindow(QMainWindow):
    def __init__(self):
        super(TableWindow, self).__init__()
        self.ui = Ui_TableWindow()
        self.ui.setupUi(self)
        self.ui.get_tables_button.clicked.connect(self.create_list)
        self.ui.listWidget.itemDoubleClicked.connect(self.create_table)
        self.ui.tableWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.tableWidget.customContextMenuRequested.connect(self.onCustomContextMenuRequested)
        self.current_table_name = None
        self.current_table_arr = None
        self.ui.tableWidget.itemChanged.connect(self.update_row)

    def create_table(self, table_name=None):
        if table_name is not None:
            self.current_table_name = table_name.text()

        query_result = db.view(self.current_table_name)
        """
            query_result contains tuple with two lists
            first list is table column names
            second list is table rows
        """
        headers = query_result[0]
        items = query_result[1]
        self.current_table_arr = [[0 for i in range(len(headers))] for j in range(len(items))]
        self.ui.tableWidget.itemChanged.disconnect(self.update_row)
        self.ui.tableWidget.setRowCount(len(items))
        self.ui.tableWidget.setColumnCount(len(headers))
        self.ui.tableWidget.setHorizontalHeaderLabels(headers)
        for n, item in enumerate(items):
            for i, elem in enumerate(item):
                new_item = QTableWidgetItem(str(elem))
                self.ui.tableWidget.setItem(n, i, new_item)
                self.current_table_arr[n][i] = new_item.text()

        self.ui.tableWidget.itemChanged.connect(self.update_row)

    def create_list(self):
        tables = db.get_tables()
        self.ui.listWidget.clear()
        for table_name in tables:
            self.ui.listWidget.addItem(str(table_name[0]))

    def onCustomContextMenuRequested(self, pos):
        item = self.ui.tableWidget.itemAt(pos)
        menu = QMenu()
        if item is None:
            add_action = menu.addAction("New entry")
            menu.addAction(add_action)
            add_action.triggered.connect(self.add_row)
        else:
            delete_action = menu.addAction("Delete")
            menu.addAction(delete_action)
            delete_action.triggered.connect(lambda: self.delete_row(item))

        menu.exec_(self.ui.tableWidget.mapToGlobal(pos))

    def add_row(self):
        parameters = []
        headers_count = self.ui.tableWidget.columnCount()
        for i in range(headers_count):
            header_text = self.ui.tableWidget.horizontalHeaderItem(i).text()
            input_text = QInputDialog.getText(self, "Input dialog", f"Enter {header_text}")
            parameters.append(input_text[0])
        if db.insert(self.current_table_name, *parameters):
            self.create_table()
        else:
            QMessageBox.warning(self, 'Error', "Record isn't  added. See log.txt")

    def delete_row(self, del_item):
        row = del_item.row()

        if self.current_table_name == 'Deliveries':
            contract_number = self.ui.tableWidget.item(row, 0).text()
            item = self.ui.tableWidget.item(row, 1).text()
            deleted = db.delete(self.current_table_name, contract_number, item)
        else:
            code = self.ui.tableWidget.item(row, 0).text()
            deleted = db.delete(self.current_table_name, code)

        if deleted:
            self.create_table()
        else:
            QMessageBox.warning(self, 'Error', "Record isn't  deleted. See log.txt")

    def update_row(self, new_item):
        row = new_item.row()
        col = new_item.column()
        if self.current_table_arr[row][col] == new_item.text():
            return
        column_name = self.ui.tableWidget.horizontalHeaderItem(col).text()
        if self.current_table_name == 'Deliveries':
            if col == 0 or col == 1:
                contract_number = self.current_table_arr[row][0]
                item = self.current_table_arr[row][1]
            else:
                contract_number = self.ui.tableWidget.item(row, 0).text()
                item = self.ui.tableWidget.item(row, 1).text()
            updated = db.update(self.current_table_name, column_name, new_item.text(), contract_number, item)
        else:
            if col == 0:
                code = self.current_table_arr[row][col]
                updated = db.update(self.current_table_name, column_name, new_item.text(), code)
            else:
                code = self.ui.tableWidget.item(row, 0).text()
                updated = db.update(self.current_table_name, column_name, new_item.text(), code)
        if not updated:
            QMessageBox.warning(self, 'Error', "Record isn't changed. See log.txt")

        self.create_table()
